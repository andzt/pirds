import sys, os

if sys.version_info[0] == 2:
    from Tkinter import *
else:
    from tkinter import *

root = Tk()
frame = Frame(root, bg="black")
frame.pack(fill="both", expand =1)
w = Label(frame, text="Updating Content...", fg="white", bg="black", font=("", 32))
w.pack(anchor='center')
root.attributes("-fullscreen", True)
root.mainloop()
